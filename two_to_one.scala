object Kata {

  def longest(s1: String, s2: String): String = (s1+s2).distinct.sorted
}